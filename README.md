# E3 Tile Editor

JavaScript Tile Map Editor

### Pre-requisites

* *NodeJS*
* *Grunt-cli*
* *Electron*

### Installing

```
npm install

```

## Running

```
grunt web

```
